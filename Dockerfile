FROM rust:1.71-slim

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /

RUN ["apt-get", "update"]

# General build dependencies.
RUN ["apt-get", "-y", "-q", "install", \
    "pkg-config" \
]

# Rust toolchain components
RUN ["rustup", "component", "add", \
    "llvm-tools-preview" \
]

# Cargo binaries
RUN ["cargo", "install", "cargo-llvm-cov", "--locked"]
